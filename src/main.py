import itertools
import pandas as pd

from mip import Model, xsum, minimize, BINARY


day_names = ["Понедельник", "Вторник", "Четверг", "Пятница", "Суббота"]


class Store:

    def __init__(self, index, code, name, address, duration):
        self.index = index
        self.code = code
        self.name = name
        self.address = address
        self.duration = duration


def get_store_count_by_day(stores):
    store_count_by_day = {}
    for day_idx in range(5):
        store_count_by_day[day_idx] = 0
        for store_idx in range(len(stores)):
            if stores[store_idx].schedule[day_idx]:
                store_count_by_day[day_idx] += 1
    return store_count_by_day


class MerchantDay:
    
    def __init__(self):
        # индексы x-переменных для каждого мерче-дня
        self.xidx = []
        # индексы y-переменных для каждого мерче-дня
        self.yidx = []


def load_stores(plan, source):
    stores = []
    for index, row in plan.iterrows():
        stores.append(Store(index, row["code"], row["name"], row["address"], row["duration"]))
        
    for index, row in source.iterrows():
        assert stores[index].name == row["Сеть"]
        assert stores[index].address == row["Адрес ТТ"]
        stores[index].schedule = [int(row[day_name]) for day_name in day_names]
        stores[index].merchant = row["Мерчендайзер (ФИО)"]
    return stores


def output_visits(fp, distances, stores, merchant_days, xcoefs, mvars, xvars):
    for merch_idx, merch in enumerate(mvars):
        print(merch_idx)
        if merch < 0.99:
            continue
        for day_idx in range(5):
            day_name = day_names[day_idx]
            visited_stores = []
            store_idx = 0
            for store in stores:
                if not store.schedule[day_idx]:
                    continue
                xidx = merchant_days[merch_idx][day_idx].xidx[store_idx]
                if xvars[xidx] > 0.99:
                    visited_stores.append(store)
                store_idx += 1

            min_distance = None
            best_order = None

            for permutation in itertools.permutations(visited_stores, len(visited_stores)):
                distance = 0
                for i in range(len(permutation) - 1):
                    distance += distances[permutation[i].index][permutation[i + 1].index]
                if min_distance is None or distance < min_distance:
                    min_distance = distance
                    best_order = permutation

            total_duration = 0
            for i, visited_store in enumerate(best_order):
                if i == len(visited_stores) - 1:
                    distance = 0
                else:
                    distance = int(distances[best_order[i].index][best_order[i + 1].index])
                total_duration += distance + visited_store.duration
                fp.write(f"{day_name} {merch_idx} {visited_store.code} {visited_store.duration} {distance}\n")
            if total_duration > 570:
                print(f"ALARM: {merch_idx} {day_idx} {total_duration}")


def main():
    plan = pd.read_excel("../data/points.xlsx", sheet_name="План_посещений")
    source = pd.read_excel("../data/points.xlsx", sheet_name="Источник").fillna(0)
    dst = pd.read_excel("../data/points.xlsx", sheet_name="матрица расстояний", header=None)

    stores = load_stores(plan, source)
    distances = dst.to_numpy() / 1000

    store_count = 204
    merchant_count = 13

    stores = stores[:store_count]
    store_count_by_day = get_store_count_by_day(stores)

    merchant_days = {}
    for merch_idx in range(merchant_count):
        merchant_days[merch_idx] = [MerchantDay() for _ in range(5)]

    model = Model()

    mvars = []
    for merch_idx in range(merchant_count):
        mvars.append(model.add_var(var_type=BINARY))

    xcoefs = []
    xvars = []
    for merch_idx in range(merchant_count):
        for store_idx in range(store_count):
            for day_idx in range(5):
                if not stores[store_idx].schedule[day_idx]:
                    continue
                merchant_days[merch_idx][day_idx].xidx.append(len(xvars))
                xvars.append(model.add_var(var_type=BINARY))
                xcoefs.append(stores[store_idx].duration)

    ycoefs = []
    yvars = []
    for merch_idx in range(merchant_count):
        for src_idx in range(store_count):
            for dst_idx in range(store_count):
                if src_idx == dst_idx:
                    continue
                for day_idx in range(5):
                    if not stores[src_idx].schedule[day_idx] or not stores[dst_idx].schedule[day_idx]:
                        continue
                    merchant_days[merch_idx][day_idx].yidx.append(len(yvars))
                    yvars.append(model.add_var(var_type=BINARY))
                    ycoefs.append(distances[src_idx][dst_idx])

    for merch_idx in range(merchant_count):
        for day_idx in range(5):
            for i in merchant_days[merch_idx][day_idx].xidx:
                model += mvars[merch_idx] >= xvars[i]

    # Условие \sum_{ijk} c_{ijk} x_{ijk} + \sum_{isjk} d_{isjk} y_{isjk} <= 570
    # Условие \sum_{ijk} c_{ijk} x_{ijk} >= 300
    for merch_idx in range(merchant_count):
        for day_idx in range(5):
            model += (
                xsum(xcoefs[i] * xvars[i] for i in merchant_days[merch_idx][day_idx].xidx) +
                xsum(ycoefs[i] * yvars[i] for i in merchant_days[merch_idx][day_idx].yidx)
            ) <= 565 * mvars[merch_idx]
            model += xsum(xcoefs[i] * xvars[i] for i in merchant_days[merch_idx][day_idx].xidx) >= 300 * mvars[merch_idx]

    # Условие что только один мерчендайзер посещает магазин всю неделю
    idx = 0
    for merch_idx in range(merchant_count):
        for store_idx in range(store_count):
            for sdv in range(sum(stores[store_idx].schedule) - 1):
                model += xvars[idx] == xvars[idx + 1]
                idx += 1
            idx += 1
    assert idx == len(xvars)

    # Условие, что ровно один мерчендайзер на магазин в течение дня
    for day_idx in range(5):
        for store_idx in range(store_count_by_day[day_idx]):
            model += xsum(
                xvars[merchant_days[merch_idx][day_idx].xidx[store_idx]]
                for merch_idx in range(merchant_count)
            ) == 1

    # Условие, что из точки выехали ровно один раз
    for merch_idx in range(merchant_count):
        for day_idx in range(5):
            scbd = store_count_by_day[day_idx]
            assert scbd == len(merchant_days[merch_idx][day_idx].xidx)
            for store_idx in range(scbd):
                model += xsum(
                    yvars[merchant_days[merch_idx][day_idx].yidx[store_idx * (scbd - 1) + i]]
                    for i in range(scbd - 1)
                ) == xvars[merchant_days[merch_idx][day_idx].xidx[store_idx]]

    # Условие, что количество ребер = количество точек
    for merch_idx in range(merchant_count):
        for day_idx in range(5):
            scbd = store_count_by_day[day_idx]
            model += (
                xsum(yvars[idx] for idx in merchant_days[merch_idx][day_idx].yidx) ==
                xsum(xvars[idx] for idx in merchant_days[merch_idx][day_idx].xidx)
            )

    model.objective = minimize(xsum(mvars[i] for i in range(merchant_count)))
    model.optimize(max_nodes=30)

    for variables, name in ((mvars, "mvars"), (xvars, "xvars"), (yvars, "yvars")):
        with open(f"../data/{name}_{store_count}.txt", "w") as fp:
            for var in variables:
                fp.write(str(int(var.x > 0.99)) + "\n")

    mvars_ = []
    xvars_ = []
    for variables, name in ((mvars_, "mvars"), (xvars_, "xvars")):
        with open(f"../data/{name}_{store_count}.txt", "r") as fp:
            for l in fp:
                variables.append(int(l.strip()))

    with open(f"../data/visits_{store_count}.csv", "w") as fp:
        output_visits(fp, distances, stores, merchant_days, xcoefs, mvars_, xvars_)


if __name__ == "__main__":
    main()
